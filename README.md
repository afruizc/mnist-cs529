# Classification on MNIST

This repository contains experiments on the MNIST dataset
for the task of classification. Two different programming languages
are used to show a simple, yet flexible way of managing machine
learning workflows.

# Data

Mnist data can be found [here](http://yann.lecun.com/exdb/mnist/)

# Running experiments

`experiments` contains a folder with the experiments that can be
run. In order to run any experiment, just step into the directory
and do `make`. The pipeline will start executing, and if things
work fine, you will obtain the result of the experiment.

`classify_xgboost` contains an experiment that classifies the digits
on MNIST using xboost. Most of the scripts used on this experiment
are writte in python.

`classify_cnn` contains an experiment that classifies the digits
on MNIST using a convolutional neural netowork. Most of the
scripts used are written in lua using torch7.

# Prerequisites

If you want to run all the experiments you should have the following
libraries installed

- [sklearn](http://scikit-learn.org) ecosystem. This includes 
  [numpy](http://numpy.org) and [scipy](http://scipy.org).
- [torch](http://torch.ch) ecosystem. This include the
  [nn](http://github.com/torch/nn) and [optim](http://github.com/torch/optim)
  packages.
- [hdf5-torch]((http://github.com/deepmind/torch-hdf5)) and
  [h5py](http://scipy.org) for reading hdf5 from lua and python.
