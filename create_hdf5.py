"""
This file reads the data downloaded from the MNIST website
and stores it in hdf5. The format that I will be using for
HDF5 is very simple:

/      -> Root group (Created by default)
/train -> Group with all the training data.
    /train/imageX -> This is a dataset that stores Image x.
                     the attribute `label` has the label for the image.
/test  -> Group with all the testing data.
    /test/imageX  -> This is a dataset that stores Image x.
                     the attribute `label` has the label for the image.
"""

import os
import sys
import argparse

import numpy as np

import h5py

endian = 'big'


def read_image_file(image_file):
    """
    Reads all the images contained in `image_file`.
    The file has to follow the format described
    (here)[http://yann.lecun.com/exdb/mnist/]

    :param image_file: The name of the file
                       that contains the images.
    :type image_file: str.
    """
    images = []
    with open(image_file, 'rb') as f:
        _ = int.from_bytes(f.read(4), endian)
        n_images = int.from_bytes(f.read(4), endian)
        n_rows = int.from_bytes(f.read(4), endian)
        n_cols = int.from_bytes(f.read(4), endian)

        for _ in range(n_images):
            image = []
            for i in range(n_rows):
                for j in range(n_cols):
                    image.append(int.from_bytes(f.read(1), byteorder=endian))
            images.append(np.array(image, dtype=np.uint8))

        return np.array(images, dtype=np.uint8)


def read_label_file(label_file):
    """
    Reads the labels form a specified file.
    """
    labels = []
    with open(label_file, 'rb') as f:
        _ = int.from_bytes(f.read(4), byteorder=endian)
        n_labels = int.from_bytes(f.read(4), endian)
        for _ in range(n_labels):
            label = int.from_bytes(f.read(1), endian)
            labels.append(label)

    return np.array(labels, dtype=np.uint8)


def main(args):
    """
    Reads the images from the binary files and stores them in
    an hdf5 file.
    """
    data_folder = os.path.abspath(args.data_folder)
    if not os.path.exists(data_folder):
        print('Data folder does not exist. Download the data first '
              'and then run this script')
        sys.exit(1)

    hdf5_file = h5py.File(args.hdf5_file, 'w')
    train_images = read_image_file(os.path.join(data_folder, 'images_train'))
    train_labels = read_label_file(os.path.join(data_folder, 'labels_train'))

    # We add all the training data
    i = 1
    for img, label in zip(train_images, train_labels):
        group = hdf5_file.create_group('/train/image{}'.format(i))
        group.attrs['label'] = label
        group.create_dataset('data', data=img)
        i += 1

    test_images = read_image_file(os.path.join(data_folder, 'images_test'))
    test_labels = read_label_file(os.path.join(data_folder, 'labels_test'))
    # We add all the testing data
    i = 1
    for img, label in zip(test_images, test_labels):
        group = hdf5_file.create_group('/test/image{}'.format(i))
        group.attrs['label'] = label
        group.create_dataset('data', data=img)
        i += 1

    hdf5_file.close()
    print('File {} succesfully created'.format(args.hdf5_file))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_folder', help='folder where the data is',
                        default='data', type=str)
    parser.add_argument('--hdf5_file',
                        help='file where the data will be stored',
                        default='mnist.h5', type=str)
    main(parser.parse_args())
