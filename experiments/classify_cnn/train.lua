require 'nn'
require 'cunn'
require 'cutorch'
require 'hdf5'
require 'optim'

require 'utils'

-- Parameters
batchSize = 10
learningRate = 0.01
coefL1 = 0.9
coefL2 = 0.95
momentum = 0.9
nEpochs = 2

torch.manualSeed(1)

local cmd = torch.CmdLine()
cmd:text()
cmd:text()
cmd:text('Training simple CNN on MNIST')
cmd:text()
cmd:text('Options')
cmd:option('--model_out', 'model.bin', 'File with trained model')
cmd:option('--hdf5_path', 'mnist.h5', 'File with the data')
cmd:option('--train_labels', 'train_labels.txt', 'File with training labels')
cmd:option('--test_labels', 'test_labels.txt', 'File with testing labels')
cmd:text()

local params = cmd:parse(arg)

classes = {'1','2','3','4','5','6','7','8','9', '10'}

-- geometry: width and height of input images
geometry = {28, 28}
model = nn.Sequential()

model:add(nn.SpatialConvolutionMM(1, geometry[1], 5, 5))
model:add(nn.Tanh())
model:add(nn.SpatialMaxPooling(3, 3, 3, 3))
-- stage 2 : mean suppresion -> filter bank -> squashing -> max pooling
model:add(nn.SpatialConvolutionMM(geometry[1], geometry[1] + geometry[2], 5, 5))
model:add(nn.Tanh())
model:add(nn.SpatialMaxPooling(2, 2, 2, 2))
-- stage 3 : standard 2-layer MLP:
model:add(nn.Reshape((geometry[1] + geometry[2]) * 2 * 2))
model:add(nn.Linear((geometry[1] + geometry[2]) * 2 * 2, 200))
model:add(nn.Tanh())
model:add(nn.Linear(200, #classes))

model:add(nn.LogSoftMax())

criterion = nn.ClassNLLCriterion()

-- retrieve parameters and gradients
parameters, gradParameters = model:getParameters()

----------------------------------------------------------------------
-- get/create dataset
--


----------------------------------------------------------------------
-- define training and testing functions

-- this matrix records the current confusion across classes
confusion = optim.ConfusionMatrix(classes)


-- training function
function train(dataset)
    -- epoch tracker
    epoch = epoch or 1

    -- local vars
    local time = sys.clock()

    -- do one epoch
    print('<trainer> on training set:')
    print("<trainer> online epoch # " .. epoch .. ' [batchSize = ' .. batchSize .. ']')
    for t = 1, dataset:size(), batchSize do
        -- create mini batch
        local inputs = torch.Tensor(batchSize, 1, geometry[1], geometry[2])
        local targets = torch.Tensor(batchSize)
        local k = 1
        for i = t, math.min(t + batchSize-1, dataset:size()) do
            -- load new sample
            local sample = dataset[i]
            local input = sample[1]:clone()
            local _,target = sample[2]:clone():max(1)
            target = target:squeeze()
            inputs[k] = input
            targets[k] = target
            k = k + 1
        end
        --inputs:cuda()
        --targets:cuda()

        -- create closure to evaluate f(X) and df/dX
        local feval = function(x)
            -- just in case:
            collectgarbage()
            print('collected that shit')

            -- get new parameters
            if x ~= parameters then
                parameters:copy(x)
            end

            -- reset gradients
            gradParameters:zero()

            -- evaluate function for complete mini batch
            print('before')
            local outputs = model:forward(inputs)
            print('middle')
            local f = criterion:forward(outputs, targets)
            print('after')

            -- estimate df/dW
            local df_do = criterion:backward(outputs, targets)
            model:backward(inputs, df_do)

            -- penalties (L1 and L2):
            --local norm, sign = torch.norm, torch.sign

            ---- Loss:
            --f = f + coefL1 * norm(parameters, 1)
            --f = f + coefL2 * norm(parameters, 2) ^ 2 / 2

            ---- Gradients:
            --gradParameters:add(sign(parameters):mul(coefL1) +
                               --parameters:clone():mul(coefL2))

            -- update confusion
            for i = 1, batchSize do
                confusion:add(outputs[i], targets[i])
            end

            -- return f and df/dX
            return f, gradParameters
        end

        sgdState = {
            learningRate = learningRate,
            momentum = momentum,
            learningRateDecay = 5e-7
        }

        optim.sgd(feval, parameters, sgdState)
        xlua.progress(t, dataset:size())
    end

    -- time taken
    time = sys.clock() - time
    time = time / dataset:size()
    print("<trainer> time to learn 1 sample = " .. (time * 1000) .. 'ms')

    -- print confusion matrix
    print(confusion)
    confusion:zero()

    epoch = epoch + 1
end

----------------------------------------------------------------------
-- and train!

local mnist_data = hdf5.open(params.hdf5_path, 'r')
trainData = readDataset(mnist_data, 'train', params.train_labels)

for i = 1, nEpochs do
-- train
    train(trainData)
end
torch.save(params.model_out, model)
print('Trained model stored in ' .. params.model_out)
