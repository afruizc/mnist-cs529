require 'nn'
require 'utils'
require 'hdf5'
require 'optim'

local cmd = torch.CmdLine()
cmd:text()
cmd:text()
cmd:text('Testing simple CNN on MNIST')
cmd:text()
cmd:text('Options')
cmd:option('--model_in', 'model.bin', 'File with trained model')
cmd:option('--hdf5_path', 'mnist.h5', 'File with the data')
cmd:option('--test_labels', 'test_labels.txt', 'File with testing labels')
cmd:text()

local classes = {'1','2','3','4','5','6','7','8','9', '10'}

local params = cmd:parse(arg)
local model = torch.load(params.model_in)
local confusion = optim.ConfusionMatrix(classes)

-- test function
function test(dataset)
    -- test over given dataset
    for t = 1, dataset:size(), 10 do

        -- create mini batch
        local inputs = torch.Tensor(10, 1, 28, 28)
        local targets = torch.Tensor(10)
        local k = 1
        for i = t,math.min(t + 10 - 1, dataset:size()) do
            -- load new sample
            local sample = dataset[i]
            local input = sample[1]:clone()
            local _,target = sample[2]:clone():max(1)
            target = target:squeeze()
            inputs[k] = input
            targets[k] = target
            k = k + 1
        end

        -- test samples
        local preds = model:forward(inputs)

        -- confusion:
        for i = 1, 10 do
            confusion:add(preds[i], targets[i])
        end
    end

    -- timing

    -- print confusion matrix
    print(confusion)
    confusion:zero()
end

local mnist_data = hdf5.open(params.hdf5_path, 'r')
local testData = readDataset(mnist_data, 'test', params.test_labels)
test(testData)
