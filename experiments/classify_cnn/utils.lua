require 'torch'

function _readSet(h5, set, labelsFile)
    local samples = h5:read(set):all()
    local X = {}
    local nSamples = 0

    for k, img in pairs(samples) do
        table.insert(X, torch.reshape(img.data, 1, img.data:size(1)))
        nSamples = nSamples + 1
    end

    local y = {}
    local f = torch.DiskFile(labelsFile, 'r')
    y = f:readDouble(nSamples)

    return nn.JoinTable(1):forward(X), torch.Tensor(y) + 1 -- Fix the indexing; LUA is 1-indexed :(
end

function readDataset(h5, set, labelsFile)
    local X, y = _readSet(h5, set, labelsFile)

    X = torch.reshape(X, X:size(1), 1, 28, 28)
    y = torch.reshape(y, y:size(1))

    -- create set
    set = {}
    set.data = X[{{1, 2000}, {}, {}, {}}]
    set.labels = y[{{1, 2000}}]

    local labelvector = torch.zeros(10)

    -- Add indexing. Pretty important when, well, indexing.
    setmetatable(set, {
        __index = function(self, index)
            local input = self.data[index]
            local label = labelvector:zeros(10) -- one hot
            label[self.labels[index]] = 1 -- Shift by 1 my classes
                                              -- class 0 becomes 1 and class 9
                                              -- becomes 10
            return {input, label}
        end
    })

    function set:size()
        return set.data:size(1)
    end

    return set
end

