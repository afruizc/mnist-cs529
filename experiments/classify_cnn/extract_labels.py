"""
Hacky utility file used to extract labels
from the hdf5 file and put them into plain
textfiles. This is necessary because torch's
hdf5 library does not support meta attributes.
"""

import argparse
import h5py


def _extract_labels(h5, set_):
    """
    Utility function to reduce code duplication
    """
    labels = []
    n_samples = len(h5[set_])
    for i in range(1, n_samples+1):
        iname = 'image{}'.format(i)
        labels.append(h5[set_][iname].attrs['label'])

    return labels


def _save_to_file(labels, filename):
    """
    Saves the labels to a specified file
    """
    with open(filename, 'w') as f:
        for l in labels:
            f.write('{}\n'.format(l))


def extract_labels(h5_file):
    """
    Extracts the training and testing labels from an
    hdf5 file.
    """
    h5f = h5py.File(h5_file, 'r')
    train_labels = _extract_labels(h5f, 'train')
    test_labels = _extract_labels(h5f, 'test')

    return train_labels, test_labels


def main(args):
    """
    Runs the program
    """
    train_labels, test_labels = extract_labels(args.hdf5_path)
    _save_to_file(train_labels, args.train_out)
    _save_to_file(test_labels, args.test_out)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--hdf5_path', default='mnist.h5', type=str,
                        help='file in hdf5 format that contains the data')
    parser.add_argument('--train_out', default='train_labels.txt', type=str,
                        help='Output file for the training labels')
    parser.add_argument('--test_out', default='test_labels.txt', type=str,
                        help='Output file for the test labels.')
    main(parser.parse_args())
