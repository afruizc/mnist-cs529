import argparse

import h5py
import joblib
import numpy as np
import xgboost as xgb


def _read_imageset_fromh5(h5, set_):
    """
    Helper function that reads all the data for either
    the training or testing set. This function helps get rid
    of duplicate code.
    """
    n_samples = len(h5[set_])

    X = []
    y = []
    for i in range(1, n_samples+1):
        img_name = 'image{}'.format(i)
        X.append(np.array(h5[set_][img_name]['data']))
        y.append(h5[set_][img_name].attrs['label'])

    return np.array(X), np.array(y)


def read_hdf5(hdf5_path):
    """
    Reads the data from the hdf5 file and returns
    four arrays: X_train, y_train, X_test, y_test.
    The meanings should be obvious.
    """
    mnist_h5 = h5py.File(hdf5_path, 'r')
    X_train, y_train = _read_imageset_fromh5(mnist_h5, 'train')
    X_test, y_test = _read_imageset_fromh5(mnist_h5, 'test')

    return X_train, y_train, X_test, y_test


def create_model(train, valid, out_path, model_config=None):
    """Creates the model and saves it to `out_path`"""
    if model_config is None:
        model_config = {
            'missing': np.nan,
            'max_depth': 5,
            'n_estimators': 100,
            'learning_rate': 0.02,
            'nthread': 8,
            'subsample': 0.95,
            'colsample_bytree': 0.85,
            'seed': 1234
        }

    model = xgb.XGBClassifier(**model_config)

    X_train, y_train = train
    X_valid, y_valid = valid
    print(X_valid.shape, y_valid.shape)

    fit_config = {
        'X': X_train,
        'y': y_train,
        'eval_set': [(X_valid, y_valid)],
        'eval_metric': 'mlogloss',
        'early_stopping_rounds': 20,
        'verbose': True
    }

    model.fit(**fit_config)
    return model


def main(args):
    X_train, y_train, X_test, y_test = read_hdf5(args.hdf5_path)
    print('Data has been fetched')
    model = create_model((X_train, y_train),
                         (X_test, y_test),
                         args.model_out)
    joblib.dump(model, args.model_out)
    print('Model stored in {}'.format(args.model_out))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_out', default='model.bin', type=str,
                        help='file in which the model will be stored')
    parser.add_argument('--hdf5_path', default='mnist.h5', type=str,
                        help='file in hdf5 format that contains the data')
    main(parser.parse_args())
