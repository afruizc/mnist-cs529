import argparse

import numpy as np
import h5py
import joblib

from train import _read_imageset_fromh5


def main(args):
    model = joblib.load(args.model_in)
    f = h5py.File(args.hdf5_path)
    X_test, y_test = _read_imageset_fromh5(f, 'test')
    y_pred = model.predict(X_test)
    print('Accuracy:', np.sum(y_pred == y_test) / X_test.shape[0])

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_in', default='model.bin', type=str,
                        help='file in which the model will be stored')
    parser.add_argument('--hdf5_path', default='mnist.h5', type=str,
                        help='file in hdf5 format that contains the data')
    main(parser.parse_args())
