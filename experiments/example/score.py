import argparse

import numpy as np


def main(args):
    print(np.load(args.model_in))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_in', default='model.bin', type=str,
                        help='file in which the model will be stored')
    main(parser.parse_args())
