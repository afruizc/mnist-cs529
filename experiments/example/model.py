import argparse
import numpy as np


def main(args):
    np.save(args.model_out, np.random.rand(5, 5))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_out', default='model.bin', type=str,
                        help='file in which the model will be stored')
    main(parser.parse_args())
