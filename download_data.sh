# If the data has not been downloaded or
# extracted, this takes care of it. The argument
# should indicate where the data is to be downloaded.

if [ $# -eq 0 ]; then
    echo "You must specify a target dir"
    echo "Usage: download_data.sh [target_dir]"
    exit 1
fi

target_dir=$1

if [ ! -d "$target_dir" ]; then
    mkdir $target_dir
fi

if [ ! -f "$target_dir/images_train" ]; then
    wget http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz
    mv train-images-idx3-ubyte.gz $target_dir/images_train.gz
    gunzip $target_dir/images_train.gz

    wget http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz
    mv t10k-images-idx3-ubyte.gz $target_dir/images_test.gz
    gunzip $target_dir/images_test.gz

    wget http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz
    mv train-labels-idx1-ubyte.gz $target_dir/labels_train.gz
    gunzip $target_dir/labels_train.gz

    wget  http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz
    mv t10k-labels-idx1-ubyte.gz $target_dir/labels_test.gz
    gunzip $target_dir/labels_test.gz
fi
